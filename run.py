# coding: utf-8
#!/usr/bin/python
# Python 2.7
# Using KRust to run Rust official test suite
# put this file into run-pass folder
import re
import os
import commands
import subprocess
import time
num = 0
codes = []
# compute the number of files on this folder
for root, dir, files in os.walk(os.getcwd()):
    pattern = re.compile('.*\.rs$')
    for f in files:
        if pattern.match(f):
        #if f[-2:] == "rs":
            codes.append(root + '/' + f)
            num += 1
print(num) # num of files

success_result_pattern = re.compile(".*<k>(.*?)</k>.*")
#ret = commands.getstatusoutput("krun " + "/Users/jerry/Documents/Coding/github/rust/src/test/pretty/fn-return.rs --directory  /Users/jerry/Documents/ShanghaiTech/research/Rust/k-framework/krust")
#print ret[1]
#ret = "<T> <k> . </k> <env> main |-> 1 f |-> 0 </env> <tempEnv> main |-> 1 f |-> 0 </tempEnv> <control> <fstack> .List </fstack> </control> <genv> main |-> 1 f |-> 0 </genv> <typeEnv> .Map </typeEnv> <store> 1 |-> lambda ( .StructValues , { } , () ) 0 |-> lambda ( .StructValues , { } , () ) </store> <mutType> 1 |-> immutable 0 |-> immutable </mutType> <tempMutType> 1 |-> immutable 0 |-> immutable </tempMutType> <nextLoc> 2 </nextLoc> <out> .List </out> <borrow> .Map </borrow> <ref> .Map </ref> <refType> .Map </refType> <moved> .Map </moved> </T>"
#print success_result_pattern.match(ret).group(1)
parser_success = 0 
parser_success_codes = []
run_success = 0
run_success_codes = []
parser_success_fail_to_run  = []
start = time.time()
# print(codes)
for i in range(num):
    cmd = "krun " + codes[i] + " --directory  /Users/jerry/Documents/ShanghaiTech/research/Rust/k-framework/krust"
    #cmd = "krun " + codes[i] + " --directory /home/hello/wangfeng/krust"
    #print codes[i]
    ret = commands.getstatusoutput(cmd)
    if (i+1) % 100 == 0:
        print("Running %d/3119" % (i+1))
    #print ret
    match_result = success_result_pattern.match(ret[1])
    if match_result:
        parser_success += 1
        parser_success_codes.append(codes[i])
        if match_result.group(1).strip() == '.':
            run_success += 1
            run_success_codes.append(codes[i])
        else:
        # parse successfully and fail to run  
            parser_success_fail_to_run.append(codes[i])
end = time.time()
total_time = end - start
print("Successfully parse " + str(parser_success) + "files")
print("Successfully run " + str(run_success) + "files")
print("Total time : %.3f seconds" % total_time) 

with open('parser_success_codes.txt','w') as f:
    for code in parser_success_codes:
        f.write(code)
        f.write('\n')

with open('run_success_codes.txt','w') as f:
    for code in run_success_codes:
        f.write(code)
        f.write('\n')

with open('parser_success_fail_to_run.txt','w') as f:
    for code in parser_success_fail_to_run:
        f.write(code)
        f.write('\n')




