struct Point {
    x : i32,
    y : i32,
}

impl Point {
    fn say_hi(x:i32,y:i32) {
        println!("Hi,{},{}",x,y);
    }

    fn say_hello(self, z:i32) {
        println!("Hello,({},{},{})", self.x, self.y, z);
    }

    fn sing() {
        println!("Singing");
    }

    fn one() -> i32 {
        return 1;
    }

    fn test_ref(&self) {
        println!("{}", self.x * 100);
    }

    fn test_mutref(&mut self){
        self.x = 0;
        self.y = 0;
    }
}

fn main() {
    let mut q = Point {x:1, y:2};
    Point::say_hi(1,2);
    Point::sing();
    let one = Point::one();
    println!("{}", one);
    q.say_hello(0);
    let mut p = Point {x:10,y:100};
    p.test_ref();
    p.test_mutref();
    println!("{},{}", p.x, p.y);
    p.test_ref();
    //q.x = 1; q is moved now
}