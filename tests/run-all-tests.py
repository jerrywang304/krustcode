# coding: utf-8
#!/usr/bin/python
# Python 2.7
# Using KRust to run all testcases in tests folder
import re
import os
import commands
import subprocess
import time
num = 0
codes = []
# compute the number of files on this folder
for root, dir, files in os.walk(os.getcwd()):
    pattern = re.compile('.*\.rs$')
    for f in files:
        if pattern.match(f):
        #if f[-2:] == "rs":
            codes.append(root + '/' + f)
            num += 1
print(num) # num of files

success_result_pattern = re.compile(".*<k>(.*?)</k>.*")
#ret = commands.getstatusoutput("krun " + "/Users/jerry/Documents/Coding/github/rust/src/test/pretty/fn-return.rs --directory  /Users/jerry/Documents/ShanghaiTech/research/Rust/k-framework/krust")
#print ret[1]
#ret = "<T> <k> . </k> <env> main |-> 1 f |-> 0 </env> <tempEnv> main |-> 1 f |-> 0 </tempEnv> <control> <fstack> .List </fstack> </control> <genv> main |-> 1 f |-> 0 </genv> <typeEnv> .Map </typeEnv> <store> 1 |-> lambda ( .StructValues , { } , () ) 0 |-> lambda ( .StructValues , { } , () ) </store> <mutType> 1 |-> immutable 0 |-> immutable </mutType> <tempMutType> 1 |-> immutable 0 |-> immutable </tempMutType> <nextLoc> 2 </nextLoc> <out> .List </out> <borrow> .Map </borrow> <ref> .Map </ref> <refType> .Map </refType> <moved> .Map </moved> </T>"
#print success_result_pattern.match(ret).group(1)
count = 0 # number of successful files
start = time.time()
for i in range(num):
    cmd = "krun " + codes[i] + " --directory  /Users/jerry/Documents/ShanghaiTech/research/Rust/k-framework/krust"
   # cmd = "krun " + codes[i] + " --directory /home/hello/Downloads/krust"
    if codes[i] == "/Users/jerry/Documents/ShanghaiTech/research/Rust/k-framework/krust/tests/passed-files-from-run-pass/deep.rs":
        count += 1
        continue # deep.rs costs too much time
    ret = commands.getstatusoutput(cmd)
    #print ret
    match_result = success_result_pattern.match(ret[1])
    if match_result and match_result.group(1).strip() == '.':
        count += 1
        #print codes[i] + ": success"
    else:
        print "Failed to run : " + codes[i]
end = time.time()
total_time = end - start
print("Successfully run " + str(count) + "files")
print("in %.3f seconds" % total_time) 


# In[ ]:



