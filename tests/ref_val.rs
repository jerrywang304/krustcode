fn main() {

    let x:&mut isize = &mut 42;
    println!("{}", *x);
    let x: &isize = x;
    let x: &isize = &mut 42;
}