struct Circle {
    x: f64,
    y: f64,
    radius: f64
}

trait HasArea {
    fn area(&self) -> f64;

    fn test(&self);
    fn test2(x:i32) -> i32;
}

impl HasArea for Circle {
    fn area(&self) -> f64 {
        return self.radius * self.radius * 3.141593;
    }

    fn test(&self) {
        println!("({},{})", self.x, self.y);
    }

    fn test2(x:i32)->i32 {
        x
    }
}

fn main() {
    let c = Circle { x:0.0, y:0.0, radius:4.0};
    println!("{}", c.area());
    c.test();
    Circle::test2(2);

}
