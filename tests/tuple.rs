fn main() {
    let x = (1,2,3);
    let y = x.0;
    println!("{:?}", y);
    println!("{:?}", x.0);
    println!("{:?}", x.1);
    println!("{:?}", x.2);
    println!("{:?}", (2, 1));
    let z = x;
    println!("{:?}", z.0);
    let (a,b,c) = x;
    println!("{:?}", a);
    let p : (i32, i32, bool) = (1,2,false);
}
