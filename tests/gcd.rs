fn main() {
    
    let x = gcd(6,9);
    println!("{}",x);
}

fn gcd(a: i32, b : i32) -> i32 {
    if b == 0 { 
        return a;
    } else {
        return gcd(b, a % b);
    }

    
}