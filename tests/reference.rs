#![allow(unused_variables)]
#![allow(unused_assignments)]
fn main() {
    let mut y = 10;
    let mut z = 100;
    let mut x;
    x = &mut y;  // y must be mutable
    //x = &mut z;  // re-assign x
    //print(*x);
    // immutable
    let one = 1;
    let two = 2;
    let mut t; 
    t = &one;
    //print(*t);
    t = &two;
    let mut m;
    m = &one;
    let mut three = 3;
    let mut p = &one;
    p = &mut three;
    assert_eq!(10, *x);
    let ref a = one;
    assert_eq!(*a, 1);
    test_ref();


}

pub fn test_ref(){
    let x = 1;
    let y = 1;
    assert_eq!(&x, &y);
}