fn main() {
    let x: u8 = 10;
    assert_eq!(x, 10);
    let y: i8 = -1;
    assert_eq!(y, -1);
    let z: u32 = 9;
    assert_eq!(z, 9);
    let p: usize = 100;
    assert_eq!(p, 100);
    let q: i16 = 12;
    assert_eq!(q, 12);
    let o: u64 = 98;
    assert_eq!(o, 98);
}