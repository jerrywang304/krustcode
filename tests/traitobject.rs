struct Ball {
    radius : f64,
    weight : f64
}

struct Cube {
    width : f64,
    weight: f64
}

trait HasWeight {
    fn getWeight(&self) -> f64;
}

impl HasWeight for Ball {
    fn getWeight(&self) -> f64 {
        return self.weight;
    }
}

impl HasWeight for Cube {
    fn getWeight(&self) -> f64 {
        return self.weight;
    }
}

fn obj_weight(obj : &HasWeight) -> f64 {
    return obj.getWeight();
}

struct People {
    age : i32,
    height : f64,
}

fn main() {
    let ball = Ball {radius: 1.2, weight: 20.0};
    let cube = Cube {width: 2.3, weight: 30.0};
    let ball_weight = obj_weight(&ball);
    let p = People { age : 12, height : 20};
    let temp = obj_weight(&p);
    println!("{}", ball_weight);
}
