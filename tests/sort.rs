fn main() {
    let mut x = [3,6,1,2,9];
    // selection sort in accending order
    for i in 0..x.len() {
        let mut min = i;
        for j in i+1..x.len() {
            if x[j] < x[min] {
                min = j;
            }

        }
        // swap(x[i],x[min])
        let temp = x[i];
        x[i] = x[min];
        x[min] = temp;
    }
    // print 
    for i in 0..x.len(){
        println!("{}", x[i]);
    }
    
}