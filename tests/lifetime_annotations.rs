
struct Person<'a> {
    age : &'a u8,
}

fn main() {
    let a = String::from("123");
    let b = String::from("2dcd");
    let mut c;  
    c = test(&a, &b);
    println!("a = {}", a);
    println!("c = {}", c);
    let d = longest(&a, &b);
    println!("d = {}", d);
    let x = 18;
    let person = Person {age : &x};
    println!("{}", person.age);

}

fn test<'a> (x: &'a String, y: &'a String) -> &'a String {
//fn test(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        return x;
    }else {
        return y;
    }

}

fn longest<'a, 'b : 'a> (x: &'a String, y: &'b String) -> &'a String {
    if x.len() > y.len() {
        return x;
    }else {
        return y;
    }

}

fn test2<'a, 'b>(x:&'a String, y : &'b String) -> &'b String 
      where 'a : 'static {
            if x.len() > y.len() {
        return x;
    }else {
        return y;
    }
}
