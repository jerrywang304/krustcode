fn main() {
    let mut s = "Hi";
    println!("{}", s.len());
    assert_eq!("Hi", s);
    s = "world";
    println!("{}", s);
    let mut p: &str = "wang" ;
    println!("{}", p);
    let mut q:&'static str = "Elon";
    q = "SOS";
    println!("{}", q);
    let t = 'x';
    println!("t = {}", t);
    let mut a = String::from("Elon Musk");
    let mut b = String::from("KKK");
    let c: String = String::from("CCC");
    let mut d: String = String::from("ABCD");
    d = a;
    println!("{}", d.len());
    //b = a; Error: a is moved!

}