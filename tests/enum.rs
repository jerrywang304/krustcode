#[derive(Debug)]
enum IpAddrKind {
    V4,
    V6
}

enum Com {
    X(i32,i32),
    Y(i32)
}

fn main() {
    let a = Com::X(1,2);
    let b = Com::Y(2);

    let mut four = IpAddrKind::V4;
    let six = IpAddrKind::V6;
    let mut x = 0;
    let mut y = 10;
    match x {
        0 => println!("0"),
        1 => println!("1"),
        y => println!("10"),
        _ => println!("None"),

    }
    let k = match 3 {
        1 => 1,
        3 => 3,
        _ => 0,
    };
    // case 1
    let e1 = match six {
        IpAddrKind::V6 => 6,
        IpAddrKind::V4 => 4,
        _ => 0,
    };s
    // case 2
    let e2 = match six {
        IpAddrKind::V4 => 4,
        IpAddrKind::V6 => 6,
        _ => 0,
    };
    // case 3
    let e3 = match six {
        _ => 0,
        IpAddrKind::V4 => 4,
        
    };  
    // case 4
    let e4 = match six {
        IpAddrKind::V4 => 4,
        _ => 0,
    };    
}