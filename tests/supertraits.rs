struct Point {
    x : i32,
    y : i32,
}

trait Eq {
    fn eq(&self, &Self) -> bool;
}

trait Ord: Eq {
    fn lt(&self, &Self) -> bool;
    fn le(&self, &Self) -> bool;
}

impl Eq for Point {
    fn eq(&self, other: &Point) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

impl Ord for Point {
    fn lt(&self, other: &Point) -> bool {
        return self.x < other.x || (self.x == other.x && self.y < other.y);
    }

    fn le(&self, other: &Point) -> bool {
        return self.lt(other) || self.eq(other);
    }
}

fn main() {
    let p1 = Point {x:0,y:0};
    let p2 = Point {x:0,y:0};
    let p3 = Point {x:1,y:1};
    if p1.eq(&p2) {
        println!("Same");
    }
    if p1.lt(&p3) {
        println!("left");
    }
    
}