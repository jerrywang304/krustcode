###Formal syntax and semantic of Rust impletemented by K framework



#### In this repo

- **doc** contains some tutorials about how to install or use K framework, and some other useful links or files.
- **rs-semantics-\*.k** contains the specific semantics rules.
- **rs-syntax.k** only defines the grammar.
- **rs.k** wraps **rs-semantics-\*.k** and **rs-syntax.k**
- **tests** includes some very basic and simple Rust codes used to test all the rules. It also contains a floder called **passed-files-from-run-pass**. The codes in this folder are from Rust compiler's test suite which krust can also successfully pass.
